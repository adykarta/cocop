import {render} from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import Loading from '../Components/Loading';
import Logo from '../Assets/pokeball.png';

describe('Loading',()=>{
    it('Finds text and image',()=>{
        const {getByText, getByTestId} = render(<Loading/>);
        const elem = getByTestId('loadingText');
        const elem2 = getByTestId('loadingImg');
      
        expect(elem.innerHTML).toBe('Loading...');
        expect(elem2.src).toContain(Logo)
          
    })
})
