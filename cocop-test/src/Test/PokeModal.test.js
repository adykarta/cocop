import {render, fireEvent, wait, getByAltText} from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import PokeModal from '../Components/PokeModal';

const modal = {
    state:true,
    data:{
        name:'test',
            height:'10',
            base_experience:'200',
            abilities:[
                {
                    ability:
                        {name:'fly'}
                }
            ],
            sprites:{
                front_default:'http://pokeapi.co/media/sprites/pokemon/12.png'
            }
    }
}

const handleClose = jest.fn()

describe('PokeModal',()=>{
    it('render component props',()=>{
        const {getByTestId, getByAltText} = render( <PokeModal open={modal} handleClose ={()=>handleClose()}/>);     
        const elem = getByTestId('pokeName');
        const elem2 = getByTestId('height');
        const elem3 = getByTestId('base');
        const elem4 = getByTestId('ability');
        const elem5 = getByAltText('poke-img');
      

        expect(elem.innerHTML).toBe(modal.data.name);
        expect(elem2.innerHTML).toContain(modal.data.height);
        expect(elem3.innerHTML).toContain(modal.data.base_experience);
        expect(elem4.innerHTML).toContain(modal.data.abilities[0].ability.name);
        expect(elem5.src).toContain(modal.data.sprites.front_default);
          
    })
    it('hit close modal button',()=>{
        const {getByText} = render( <PokeModal open={modal} handleClose ={()=>handleClose()}/>);     
        fireEvent.click(getByText('Close'));
        expect(handleClose).toHaveBeenCalled();
             
    })

})
