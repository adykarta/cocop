import {render, fireEvent, waitForElement} from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';
import PokedexCard from '../Components/PokedexCard';

const datas= [
    {
        data:{
            id:1,
            name:'test',
            height:'10',
            base_experience:'200',
            abilities:[
                {
                    ability:
                        {name:'fly'}
                }
            ],
            sprites:{
                front_default:'http://pokeapi.co/media/sprites/pokemon/12.png'
            }

        }
       
    }
]
describe('Pokedex Card', ()=>{
    it('render pokedex card', ()=>{
        const {getByTestId, getByAltText} = render(   <PokedexCard data ={datas} />); 
        const elem = getByTestId('poke-name');
        const elem2 = getByAltText('pokeImg');

        expect(elem.innerHTML).toBe(datas[0].data.name);
        expect(elem2.src).toContain(datas[0].data.sprites.front_default);
    })
    it('open details button', async()=>{
        const {getByTestId, getByAltText, getByText} = render(   <PokedexCard data ={datas} />); 
        fireEvent.click(getByText('See Details'));

        const elem =  await waitForElement(()=>getByTestId('pokeName'));
        const elem2 =  await waitForElement(()=>getByTestId('height'));
        const elem3 =  await waitForElement(()=>getByTestId('base'));
        const elem4 =  await waitForElement(()=>getByTestId('ability'));
        const elem5 =  await waitForElement(()=>getByAltText('poke-img'));

        expect(elem.innerHTML).toBe(datas[0].data.name);
        expect(elem2.innerHTML).toContain(datas[0].data.height);
        expect(elem3.innerHTML).toContain(datas[0].data.base_experience);
        expect(elem4.innerHTML).toContain(datas[0].data.abilities[0].ability.name);
        expect(elem5.src).toContain(datas[0].data.sprites.front_default);
    } )
})