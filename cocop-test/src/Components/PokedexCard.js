import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import {Button, Box} from '@material-ui/core';
import PokeModal from './PokeModal';
import TablePagination from '@material-ui/core/TablePagination';


const useStyles = makeStyles({
  root: {
    width: '100%',
   
  },
  container: {
    maxHeight: 440,
  },
  card:{
    width:'100%',
    display:'flex',
    flexWrap:'wrap',


  },
  pokeBox:{
    width:'20%',
    minHeight:'100px',
    backgroundColor:'red',
    margin:'1.5em',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column',
    border:'1px solid red',
    borderRadius:'8px',
    paddingBottom:'2em'
    
  }
});

export default function PokedexCard(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = React.useState({
    state:false,
    data:{}
  });
  const handleOpen = (param) => {
  
    setOpen({state:true,data:param});
  };

  const handleClose = () => {
    setOpen({state:false,data:{}});
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
            <Box className={classes.card}>
              {props.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row,idx) => {
                return (
                

                    <Box key={row.data.id} className={classes.pokeBox} >
                      <img src={row.data.sprites.front_default} alt="pokeImg"></img>
                      <h5 style={{color:'white', fontWeight:'bold'}} data-testid="poke-name">
                        {row.data.name}

                      </h5>
                      
                      <Button onClick={()=>handleOpen(row.data)} variant="contained"  style={{backgroundColor:"white", color:'black', marginTop:'0.5em', width:'50%', fontWeight:'bolder',fontSize:'0.6rem'}} >
                        See Details
                      </Button>
                    
                    </Box>
                    
                    )
                
              })}
            </Box>
     
   
      <TablePagination
        rowsPerPageOptions={[10, 25]}
        component="div"
        count={props.data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      <PokeModal open={open} handleClose ={()=>handleClose()}/>
    </Paper>
  );
}
