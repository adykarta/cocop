import React,{Fragment} from 'react'
import logo from '../Assets/pokeball.png'
import '../App.css';

export default function Loading(){
    return(
        <Fragment>
            <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', textAlign:'center'}}>
               
                <img data-testid="loadingImg" src={logo} className="loading" alt="pokeball" />
                <h2 data-testid="loadingText" style={{fontWeight:'bolder'}}>Loading...</h2>
            </div>
        </Fragment>
    )
}