import React, {Fragment, useEffect} from 'react'
import PokedexCard from '../Components/PokedexCard'
import axios from 'axios';
import { makeStyles, fade } from '@material-ui/core/styles';
import {Box, Typography, FormControl, InputLabel, Select, InputBase} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Loading from '../Components/Loading';
import pokemon from '../Assets/pokemon.png'



const useStyles = makeStyles(theme=>({
    container: {
      width: '100%',
      textAlign:'center'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
      search: {
        position: 'relative',
        display:"flex",
        alignItems:'center',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade('#92929D', 0.45),
        '&:hover': {
          backgroundColor: fade('#92929D', 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(3),
          width: 'auto',
        },
      },
      searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color:'#92929D'
      },
      inputRoot: {
        color: 'black',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
      },
    
   
  }));
  


const Dashboard = ()=>{
    const classes = useStyles();
    const [dataDetails, setDataDetails] = React.useState([])
    const [dataDetailsFiltered, setDataDetailsFiltered] = React.useState([])
    const [state, setState] = React.useState({
        base: '',
        search:''
       
      });
 
    const handleChange = (form)=>(event) => {
        const name = form;
        setState({
          ...state,
          [name]: event.target.value,
        });
       
        handleFilter(event.target.value)
    };

    useEffect(()=>{
        axios.get(`https://pokeapi.co/api/v2/pokemon?limit=1000`)
        .then(res => {
            const data = res.data.results
            const dataUrl = Array.from(data,x=>x.url)
            Promise.all(dataUrl.map(u=>axios.get(u))).then(responses =>
                {
                setDataDetails(responses)
                setDataDetailsFiltered(responses)
                }
              
            )
            
       
      })

    },[])

    const handleFilter = (param)=>{
       
        var filteredData =  dataDetails.filter(function(pokemon) {
            if(param==='<200'){
                setState(prev=>({...prev,search:''}))
                return pokemon.data.base_experience < 200;
            }
            else if(param ==='>=200'){
                setState(prev=>({...prev,search:''}))
                return pokemon.data.base_experience >= 200;
            }
            else if(param===''){
                setState(prev=>({...prev,search:''}))
                return pokemon;
            }
            else{
                return pokemon.data.name.includes(param)
            }

        });
        setDataDetailsFiltered(filteredData)

    }

 
    return(
        <Fragment>
            <Box className={classes.container}>
                <Typography variant="h1" style={{fontWeight:'bolder', marginBottom:'0.5em'}}>
                    POKEDEX
                </Typography>
                {
                    dataDetails.length===0 ?
                    <Loading/>
                    :
                    <div>
                        <Box style={{display:'flex', flexDirection:'row', alignItems:'center', marginLeft:'2em', justifyContent:'space-between'}}>
                            <Box style={{display:'flex', flexDirection:'row', alignItems:'center'}}>
                                <Typography variant="h5">Filter by:</Typography>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel htmlFor="base-experience">Experience</InputLabel>
                                    <Select
                                    native
                                    value={state.base}
                                    onChange={handleChange('base')}
                                    inputProps={{
                                        name: 'base',
                                        id: 'base-experience',
                                    }}
                                    >
                                    <option aria-label="None" value="" />
                                    <option value={'<200'}>less than 200</option>
                                    <option value={'>=200'}>more than 200</option>
                                    
                                    </Select>
                                </FormControl>
                            </Box>
                            <div className={classes.search} style={state.base===''? {display:'block'}:{display:'none'}}>
                                <div className={classes.searchIcon}>
                                <SearchIcon />
                                </div>
                                <InputBase
                                placeholder="Search by Name…"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                                inputProps={{ 'aria-label': 'search' }}
                                value={state.search}
                                onChange={handleChange('search')}
                                />
                            </div>

                        </Box>
                        {
                            dataDetailsFiltered.length ===0 ?
                            <Box style={{display:'flex', flexDirection:'column', alignItems:'center', justifyContent:'center', textAlign:'center'}}>
                                <img src={pokemon}  alt="pokemon" />
                                <Typography variant="h5" style={{fontWeight:'bold'}}>Not Found...</Typography>
                            </Box>

                            :
                            <PokedexCard data ={dataDetailsFiltered} />

                        }
                       
                    </div>
                }
              
            </Box>
        </Fragment>
    )
}

export default Dashboard
